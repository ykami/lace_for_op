#!/usr/bin/env perl
$pdflatex            = 'pdflatex -synctex=1 -halt-on-error -shell-escape';
#$latex_silent     = 'pdflatex -synctex=1 -halt-on-error -interaction=batchmode -shell-escape';
$max_repeat       = 3;
$pdf_mode	  = 1; # If one generates pdf via dvipdfmx, then choose `3'.

# Prevent latexmk from removing PDF after typeset.
# This enables Skim to chase the update in PDF automatically.
$pvc_view_file_via_temporary = 0;

# Use Skim as a previewer
#$pdf_previewer    = okular;
